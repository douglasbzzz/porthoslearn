<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 10:06
 */

    session_start();

    function getRecord(){
        /*try{
            $findA = array();
            $filter = ['_id']=new MongoDB\BSON\ObjectId($id);
            $command = [

                'find'=>'porthosArticles',
                'filter'=>$filter

            ];
            $query = new MongoDB\Driver\Command($command);

        }catch (Exception $e){

        }*/
        $connection = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        //$filter = ["idusuario"=>"16"];
        /* the following condition, and others similar to it, work as well

        $filter = ["age"=>["$gt"=>"18"]]; /*/
        //$options = []; /* put desired options here, should you need any */
        /*$options = [
            'projection' => ['_id' => 0],
            'sort' => ['x' => -1],
        ];*/
        /*$query = new MongoDB\Driver\Query($filter);
        try{
            $documents = $connection->executeQuery("porthos.porthosArticles",$query);
            foreach($documents as $document){
                $document = json_decode(json_encode($document),true);
                echo $document['descricaolonga'];
            }
        }catch (MongoException $e){
            echo $e->getMessage();
        }*/

        $filter = [
            "idusuario"=>$_SESSION['idusuario']
        ]; //qualquer campo que voce queira encontrar no documento. seria o where do sql.
        $options = [
            "skip"=>0/*,
            "limit"=>1*/

        ]; //a ordenação do negocio, por exemplo. agregação, group e afins.

        $query = new MongoDB\Driver\Query($filter,$options);

        $returnValue = $connection->executeQuery("porthos.porthosArticles",$query);
        foreach ($returnValue as $document){
            echo "Autor: ".$document->idusuario."<br/>";
            echo "Descrição breve para filtro: ".$document->descricaobreve."<br/>";
            echo "Data de Criação:".$document->data."<br/>";
            echo "<br/>";
            echo "<br/>";
            echo "Conteúdo Full: <br/>";
            echo $document->descricaolonga;
        }


    }

?>