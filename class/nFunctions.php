<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 15:05
 */
//session_start();
require_once("DB.php");
//autentica usuário no db byDouglas
function authUser($user, $pass){
    date_default_timezone_set("America/Sao_Paulo");
    /*$instrucao = "select u.id as idUser, u.nome, u.login, u.email, u.ativo as situacao, d.nome as departamento
                      from usuario as u
                      inner join departamento as d on d.id = u.iddepartamento
                      where ((login = '".$user."' and senha = '".sha1($pass)."') and u.ativo = 1)";*/
    $instrucao = "select u.id as iduser, u.nome as userfirstname, concat(u.nome, ' ', u.sobrenome) as usernamefull, u.email, u.ativo
                  from usuario as u
                  where (((u.login = '".$user."' or u.email='".$user."') and u.senha = '".sha1($pass)."') and u.ativo = 1)";
    $pdo = DB::getInstance();
    $result = $pdo->query($instrucao);
    if($result->rowCount()){
        $lines = $result->fetch(PDO::FETCH_OBJ);
        //session_start();
        $_SESSION['idusuario'] = $lines->iduser;
        $_SESSION['userfirstname'] = $lines->userfirstname;
        $_SESSION['usernamefull'] = $lines->usernamefull;
        $_SESSION['email'] = $lines->email;
        $_SESSION['situacao'] = $lines->ativo;
        $_SESSION['datahoralogin'] = date("Y-m-d H:i:s");

        //cria log;
        gravaLogAcesso($_SESSION['idusuario']);
        return 1;
    }else{
        return 0;
    }
}
function authSession($in , $now , $differenceFormat = '%i' ){
    $datetime1 = date_create($in);
    $datetime2 = date_create($now);

    $interval = date_diff($datetime2, $datetime1);

    if($interval->format($differenceFormat)<=45){
        return 1;
        //echo $interval->format($differenceFormat);
    }else{
        session_destroy();
        return 0;
        //echo $interval->format($differenceFormat);
    }
} //valida se o usuario esta logad a menos de 45min senao, derruba sessao
function gravaLogAcesso($idUsuario){
    date_default_timezone_set("America/Sao_Paulo");
    $instrucao = "insert into logacesso (idusuario, login) values ($idUsuario, '".date("Y-m-d H:i:s")."')";
    $pdo = DB::getInstance();
    try{
        $pdo->query($instrucao);
    }catch (PDOException $e){
        echo "Houve um problema para registrar o Log de Acesso. Erro.: ".$e->getMessage();
    }
}
?>