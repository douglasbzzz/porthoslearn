<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 09:03
 */

require_once ("Crud.php");

class Usuario extends Crud {
    protected $table = "usuario";

    private $name;
    private $lastName;
    private $login;
    private $email;
    private $pass;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param mixed $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }





    public function insert(){
        $sql = "INSERT INTO $this->table(nome, sobrenome, login, email, senha) VALUES(:nome, :sobrenome, :login, :email, :senha)";
        $stmt = DB::prepare($sql);
        $stmt ->bindParam(':nome',$this->name);
        $stmt ->bindParam(':email',$this->email);
        $stmt ->bindParam(':sobrenome',$this -> lastName);
        $stmt ->bindParam(':login',$this->login);
        $stmt ->bindParam(':senha',$this->pass);
        return $stmt->execute();
    }

    public function update($id){
        $sql = "UPDATE $this->table SET nome = :nome, email = :email WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(':nome',$this->name);
        $stmt->bindParam(':email',$this->email);
        $stmt->bindParam(':id',$id);
        return $stmt->execute();
    }


}